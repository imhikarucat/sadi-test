package service;

import model.Answer;
import model.Applicant;
import model.Application;
import model.Question;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by CoT on 12/2/17.
 */
@Transactional
@Service
public class ApplicantService {

    @Autowired
    private SessionFactory sessionFactory;

    public int addApplicant(Applicant applicant){

        //as when we post a question in json format: id of answer is not available
        for (Application application: applicant.getApplications()) {
            application.setApplicant(applicant);
        }

        int id = (Integer) sessionFactory.getCurrentSession().save(applicant);

        return id;
    }

    public void updateApplicant(Applicant applicant){
        sessionFactory.getCurrentSession().saveOrUpdate(applicant);
    }

    public Applicant getApplicant(int id){
        return (Applicant) sessionFactory.getCurrentSession().get(Applicant.class, id);
    }

    public List<Applicant> getAllApplicant(){
        return sessionFactory.getCurrentSession().createQuery("from Applicant").list();
    }

}
