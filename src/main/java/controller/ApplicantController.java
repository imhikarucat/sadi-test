package controller;

import model.Applicant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.ApplicantService;

import java.util.List;

@RestController
public class ApplicantController {

    @Autowired
    private ApplicantService applicantService;
    
    @RequestMapping(path = "/applicants", method = RequestMethod.POST)
    public int addApplicant(@RequestBody Applicant applicant){
        int id = applicantService.addApplicant(applicant);
        return id;
    }

    @RequestMapping(path = "/applicants", method = RequestMethod.PUT)
    public int updateApplicant(@RequestBody Applicant applicant){
        applicantService.updateApplicant(applicant);
        return applicant.getId();
    }

    @RequestMapping(path = "/applicants", method = RequestMethod.GET)
    public List<Applicant> getApplicants(){
        return applicantService.getAllApplicant();
    }
}
